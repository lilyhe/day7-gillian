package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.controller.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static com.thoughtworks.springbootemployee.Repository.Data.*;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;

@SpringBootTest
@AutoConfigureMockMvc

public class EmployeeControllerTest {
    @Autowired
    MockMvc client;

    @BeforeEach
    void setUp() {
        employeesList.clear();
    }


    @Test
    void should_get_all_employees_when_perform_get_all_employees_given_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000);
        employeesList.add(employee);
     
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lucy"));

    }

    @Test
    void should_return_the_employee_when_perform_create_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 3000);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);

        employeesList.add(employee);

        //when
        //then
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"));
    }

    @Test
    void should_return_employee_when_perform_get_employees_by_gender_given_gender() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy", 20, "female", 3000);
        employeesList.add(employee1);

        Employee employee2 = new Employee(2L, "Jack", 21, "male", 3000);
        employeesList.add(employee2);

        Employee employee3 = new Employee(3L, "Lily", 20, "female", 3000);
        employeesList.add(employee3);


        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees/?gender={gender}", "female"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$..name", containsInAnyOrder("Lucy", "Lily")));
    }

    @Test
    void should_return_specific_employee_when_perform_get_employee_by_id_given_id() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy", 20, "female", 3000);
        employeesList.add(employee1);

        Employee employee2 = new Employee(2L, "Jack", 21, "male", 3000);
        employeesList.add(employee2);

        Employee employee3 = new Employee(3L, "Lily", 20, "female", 3000);
        employeesList.add(employee3);

        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", employee2.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Jack"));

    }

    @Test
    void should_delete_employee_when_perform_delete_employee_by_id_given_id() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy", 20, "female", 3000);
        employeesList.add(employee1);

        Employee employee2 = new Employee(2L, "Jack", 21, "male", 3000);
        employeesList.add(employee2);

        Employee employee3 = new Employee(3L, "Lily", 20, "female", 3000);
        employeesList.add(employee3);

        //when
        //then
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", employee2.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

    }


}
