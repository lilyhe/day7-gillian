package com.thoughtworks.springbootemployee.controller;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.thoughtworks.springbootemployee.Repository.Data;

@RequestMapping("/companies")
@RestController
public class CompanyController {
    @PostMapping
    public Company addCompany(@RequestBody Company company) {
        company.setId(generateId());
        Data.companiesList.add(company);
        return company;
    }

    public Long generateId() {
        return Data.companiesList.stream()
                .max(Comparator.comparing(Company::getId))
                .map(company -> company.getId() + 1)
                .orElse(1L);
    }

    @GetMapping
    public List<Company> getAllCompanies() {
        return Data.companiesList;
    }

    @GetMapping("/{id}")
    public Company getSpecificCompanyById(@PathVariable Long id) {
        return Data.companiesList.stream()
                .filter(company -> (id).equals(company.getId()))
                .findFirst()
                .orElse(null);
    }

    @PutMapping("/{id}")
    public Company updateCompanyName(@PathVariable Long id, @RequestBody Company company) {
        Company updatedCompany = getSpecificCompanyById(id);
        updatedCompany.setName(company.getName());
        return updatedCompany;
    }

    @DeleteMapping("/{id}")
    public void deleteSpecificCompanyById(@PathVariable Long id) {
        Company deletedCompany = getSpecificCompanyById(id);
        Data.companiesList.remove(deletedCompany);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getSpecificPageCompanies(@RequestParam int page, @RequestParam int size) {
        if (page <= 0 || size <= 0) return new ArrayList<>();
        else return Data.companiesList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getAllEmployeesInSpecificCompany(@PathVariable Long id) {
        Company targetCompany = getSpecificCompanyById(id);
        return Data.employeesList.stream()
                .filter(employee -> employee.getCompany().equals(targetCompany.getName()))
                .collect(Collectors.toList());
    }

}
