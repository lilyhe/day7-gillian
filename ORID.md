# day7-Gillian

## Objective: 
1.We drew concept maps to enhance our understanding of OO, TDD and Refactor.
2.We learned concepts and fundamentals about HTTP and RESTful.
3.We learned how to use SpringBoot and test our code with Postman.
4.When practicing SpringBoot, I experienced pair programming and gained a lot.

## Reflective: 
Interesting but very unfamiliar with SpringBoot.

## Interpretive: 
While I'm not used to others watching me write code, pair programming has taught me a lot. Since I've never used SpringBoot, I had a slow start. But my paired student has a good master of SpringBoot. He answers a lot of my questions and helps me familiarize with SpringBoot. If I search on the Internet, it will takes more time.

## Decisional: 
When practicing springboot, type out full annotations manually so that I can get familiar with SpringBoot faster. Of course I don't have to do that when I get familiar with it. It always takes a little longer at first.











