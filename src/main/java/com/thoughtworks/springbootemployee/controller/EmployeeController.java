package com.thoughtworks.springbootemployee.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.thoughtworks.springbootemployee.Repository.Data;

@RequestMapping("/employees")
@RestController
public class EmployeeController {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee creatEmployee(@RequestBody Employee employee) {
        employee.setId(generateId());
        Data.employeesList.add(employee);
        return employee;
    }

    public Long generateId() {
        return Data.employeesList.stream()
                .max(Comparator.comparing(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getEmployeesByGender(@RequestParam String gender) {
        return Data.employeesList.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Employee getEmployeeByIdApi(@PathVariable Long id) {
        return getEmployeeById(id);
    }

    @GetMapping
    public List<Employee> getAllEmployees() {
        return Data.employeesList;
    }

    @PutMapping("/{id}")
    public Employee updateEmployeeAgeSalaryCompany(@PathVariable("id") Long id, @RequestBody Employee employee) {
        Employee updateEmployee = getEmployeeById(id);
        updateEmployee.setAge(employee.getAge());
        updateEmployee.setSalary(employee.getSalary());
        updateEmployee.setCompany(employee.getCompany());
        return updateEmployee;
    }

    public Employee getEmployeeById(Long id) {
        return Data.employeesList.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Long id) {
        Data.employeesList.remove(getEmployeeById(id));
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        if (page <= 0 || size <= 0) return new ArrayList<>();
        else return Data.employeesList.stream()
                .skip((long)(page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }
}
