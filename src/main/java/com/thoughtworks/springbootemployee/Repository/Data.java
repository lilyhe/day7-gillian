package com.thoughtworks.springbootemployee.Repository;

import com.thoughtworks.springbootemployee.controller.Company;
import com.thoughtworks.springbootemployee.controller.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


public class Data {
    public static List<Employee> employeesList = new ArrayList<>();
    public static List<Company> companiesList = new ArrayList<>();
}
